var data_general;

var ciudades = [];

var tipos = [];

$.get("data-1.json", function ($data) {

    data_general = $data;

    //Poblar la vista inicial
    $data.forEach(function ($item) {

        $(".resultados").append(generar_vista($item));

    })


    $.get("GetData.php", function ($data) {


        $(".guardados").html("");

        if (JSON.parse($data).status != "Error") {
            $array_id = JSON.parse($data).data;
            data_general.forEach(function ($item) {
                if ($array_id.includes(String($item.Id))) {
                    $(".guardados").append(generar_vista($item));
                }

            })
        }

    });

    //Pobla el Select de ciudades
    $data.forEach(function ($item) {
        if (!ciudades.includes($item.Ciudad)) {
            ciudades.push($item.Ciudad)
            $html = `<option value="${$item.Ciudad}">${$item.Ciudad}</option>`
            $("#selectCiudad").append($html);
        }

    })

    //Pobla el Select de tipos
    $data.forEach(function ($item) {
        if (!tipos.includes($item.Tipo)) {
            tipos.push($item.Tipo)
            $html = `<option value="${$item.Tipo}">${$item.Tipo}</option>`
            $("#selectTipo").append($html);
        }

    })

});

$(function () {
    $("#formulario").submit(function (event) {

        event.preventDefault();

        $(".resultados").html("");

        var ciudad = $("#selectCiudad").val();

        var tipo = $("#selectTipo").val();

        var precio = $("#rangoPrecio").val().split(";");

        data_general.forEach(function ($item) {

            if (
                (ciudad === "" || $item.Ciudad === ciudad) &&
                (tipo === "" || $item.Tipo === tipo) &&
                (formatPrecio($item.Precio) >= precio[0] && formatPrecio($item.Precio) <= precio[1])
            ) {
                $(".resultados").append(generar_vista($item));
            }
        })

    });

    $(document).on('submit', '.formubien', function (event) {

        event.preventDefault();

        $.post("Data.php", $(this).serialize(), function ($data) {

            alert(JSON.parse($data).message);

            if (JSON.parse($data).status != "Error") {
                $array_id = JSON.parse($data).data;
                data_general.forEach(function ($item) {
                    if ($array_id.includes(String($item.Id))) {
                        $(".guardados").append(generar_vista($item));
                    }

                })
            }

        });

    })
});


function generar_vista($item) {
    return `
            <div class="col s12 m6">
                <div class="card horizontal">
                    <div class="card-image">
                        <img src="img/home.jpg">
                    </div>
                    <div class="card-stacked">
                        <div class="card-content">
                            <p class="textobienes"><b>Direccion:</b>${$item.Direccion}</p>
                            <p class="textobienes"><b>Ciudad:</b>${$item.Ciudad}</p>
                            <p class="textobienes"><b>Telefono:</b>${$item.Telefono}</p>
                            <p class="textobienes"><b>Tipo:</b>${$item.Tipo}</p>
                            <p class="textobienes"><b>Precio:</b>${$item.Precio}</p>
                        </div>
                        <div class="card-action">
                        <form method="post" action="archi.php" class="formubien" id="bien${$item.Id}">                        
                            <input type="hidden" name="id" value="${$item.Id}"/>                        
                            <input type="hidden" name="direccion" value="${$item.Direccion}"/>                        
                            <input type="hidden" name="ciudad" value="${$item.Ciudad}"/>
                            <input type="hidden" name="telefono" value="${$item.Telefono}"/>
                            <input type="hidden" name="tipo" value="${$item.Tipo}"/>
                            <input type="hidden" name="codigo" value="${$item.Codigo_Postal}"/>
                            <input type="hidden" name="precio" value="${$item.Precio}"/>
                            <button type="submit" class="btn-large">Guardar</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>`
}

function formatPrecio($precio) {

    return parseInt($precio.replace("$", "").replace(",", "").trim());

}
