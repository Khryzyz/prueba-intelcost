<?php
	include_once "Conn.php";

	if ($result = $mysqli->query("SELECT id FROM vivienda")) {

		$cnt = $result->num_rows;

		if ($cnt > 0) {

			while ($fila = $result->fetch_assoc()) {
				$array_id[] = $fila["id"];
			}

			$transaction["status"] = "Success";
			$transaction["data"]   = $array_id;


			$result->close();

			echo json_encode($transaction);
		}
	} else {

		$transaction["status"] = "Error";

		echo json_encode($transaction);

	}
